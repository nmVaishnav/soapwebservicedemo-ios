//
//  ViewController.h
//  SOAP Messages Demo
//
//  Created by Naman on 8/14/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<NSXMLParserDelegate>

@property (weak, nonatomic) IBOutlet UITextField *celciousText;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

